<?php

namespace backend\models;

use common\models\active_record\ArithmeticOperations;
use yii\base\Model;
use yii\web\NotFoundHttpException;

/**
 * OperationAvailabilityForm
 */
class OperationAvailabilityForm extends Model
{
    /** @var int */
    public $id;

    /** @var int */
    public $available;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'available'], 'required'],
            [['available'], 'integer'],
            ['id', 'exist',
                'targetClass' => ArithmeticOperations::class,
                'message' => 'Нет такой операции'
            ],
        ];
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     */
    public function save() {
        $operation = (new CalculationOperations)::findOne($this->id);
        $operation->available = $this->available;
        return $operation->save();
    }
}
