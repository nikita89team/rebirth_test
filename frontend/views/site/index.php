<?php

/* @var $this yii\web\View */
/* @var $operationResult string */
/* @var $model \backend\models\CalculatorModel */

$this->title = 'Калькулятор';

?>
<div class="site-index">


    <div class="body-content">
        <div class="row">
            <div class="col-lg-12">
                <h2>Калькулятор</h2>
                <p>
                    <?= $operationResult ?>
                </p>

                <?php
                $form = \yii\widgets\ActiveForm::begin([
                    'method' => 'get',
                ]);
                ?>
                <?= $form->field($model, 'operandA') ?>
                <?= $form->field($model, 'operandB') ?>
                <?= $form->field($model, 'operation')->radioList($model->getOperations()); ?>
                <?= \yii\helpers\Html::submitButton('Выполнить', ['class' => 'btn btn-primary']) ?>


                <?php \yii\widgets\ActiveForm::end() ?>

            </div>

        </div>

    </div>
</div>
