<?php
/**
 * Created by PhpStorm.
 * User: Nikita
 * Date: 14.10.2021
 * Time: 11:26
 */

namespace frontend\components;


class BcBigNumberCalculator extends BigNumberCalculator
{
    public function addition($operand1, $operand2) {
        return bcadd($operand1, $operand2);
    }

    public function subtraction($operand1, $operand2) {
        return bcsub($operand1, $operand2);
    }
}