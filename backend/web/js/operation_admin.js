(function() {

    $(document).on('click', '.js-change-operation-avalabilty', function () {
        var el = $(this);
        el.prop('disable', true);
        $.post(el.data('url'), {
                id: el.data('id'),
                available: el.prop('checked') ? 1 : 0
            }
        ).done(function(data) {
            el.prop('disable', false);
        });
    })
})();