<?php

namespace frontend\controllers;

use frontend\components\BcBigNumberCalculator;
use frontend\models\CalculatorModel;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $operationResult = '';
        $model = new CalculatorModel([
            'calculator' => new BcBigNumberCalculator()
        ]);
        if($model->load(Yii::$app->request->get()) && $model->validate()) {
            $operationResult = $model->perform();
        }

        return $this->render('index' , [
            'model' => $model,
            'operationResult' => $operationResult,
        ]);
    }
}
