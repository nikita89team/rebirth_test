<?php

namespace frontend\models;

use common\models\active_record\ArithmeticOperations;
use frontend\components\BigNumberCalculator;
use yii\base\Model;
use yii\web\BadRequestHttpException;

/**
 * CalculatorModel
 */
class CalculatorModel extends Model
{

    /**
     * @var BigNumberCalculator
     */
   public $calculator;

    /** @var int */
    public $operandA;

    /** @var int */
    public $operation;

    /** @var int */
    public $operandB;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['operandA', 'operandB', 'operation'], 'required'],
            [['operandA', 'operandB'], 'integer'],
            ['operation', 'exist',
                'targetClass' => ArithmeticOperations::class,
                'targetAttribute' => ['operation' => 'code'],
                'message' => 'Нет такой операции'
            ],
        ];
    }


    public function attributeLabels()
    {
        return [
            'operandA' => 'Операнд',
            'operandB' => 'Операнд',
            'operation' => 'Операция',
        ];
    }


    /**
     * @return string
     * @throws BadRequestHttpException
     */
    public function perform() {
        if(method_exists($this->calculator, $this->operation)) {
            return (string) $this->calculator->{$this->operation}($this->operandA, $this->operandB);
        }
        else{
            throw new BadRequestHttpException("опеарция {$this->operation} не реализована в " . get_class($this->calculator));
        }
    }

    public function getOperations() {
        $result = [];
        $operations = ArithmeticOperations::find()->select('code')->asArray()->all();
        foreach ($operations as $o) {
            $result[$o['code']] = \Yii::t('app', $o['code']);
        }

        return $result;
    }
}
