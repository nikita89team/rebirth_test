<?php

/* @var $this yii\web\View */

/* @var $calculatorDataProvider \yii\data\BaseDataProvider */

$this->title = 'Калькулятор';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
                <h2>Калькулятор</h2>
                <?php
                    echo \yii\grid\GridView::widget([
                        'dataProvider' => $calculatorDataProvider,
                        'columns' => [
                            'id',
                            'code',
                            [
                                'class' => 'yii\grid\DataColumn',
                                'header' => 'available',
                                'content' => function($model, $key, $index, $column) {
                                    return '<input data-url="' . \yii\helpers\Url::to('site/change-operation') . '" data-id="'
                                        . $model->id . '" class="js-change-operation-avalabilty" type="checkbox" '
                                        . ($model->available ? 'checked' : '') . ' />';
                                }
                            ],
                        ]
                    ]);
                ?>
            </div>

        </div>

    </div>
</div>
