/* создание юзера */
CREATE USER IF NOT EXISTS 'nikita'@'%' IDENTIFIED BY '123456';

CREATE USER IF NOT EXISTS 'nikita2'@'localhost' IDENTIFIED BY '123456';


/* Создание бд */
CREATE DATABASE test;

/* Переход на бд */
USE test;

/* Создание таблицы */

CREATE TABLE calculator_operations (
    id TINYINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    code VARCHAR(100) NOT NULL UNIQUE KEY,
    available TINYINT DEFAULT 0
)
ENGINE=InnoDB
COMMENT="Таблица доступных операций вычисления";

GRANT SELECT, UPDATE, INSERT ON test.calculator_operations TO 'nikita'@'%';


/* запись в БД */
INSERT INTO calculator_operations (code) VALUES ('subtraction');
INSERT INTO calculator_operations (code) VALUES ('addition');