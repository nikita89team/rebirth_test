<?php

namespace backend\controllers;

use backend\models\OperationAvailabilityForm;
use common\models\active_record\ArithmeticOperations;
use common\models\LoginForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends Controller
{

    public function beforeAction($action)
    {
        if(parent::beforeAction($action)) {
            $user = Yii::$app->user->identity;
            if($user and $user->email != 'viperoussnake@yandex.ru') {
                Yii::$app->user->logout();
                $this->goHome();
                //return $this->redirect(Yii::$app->params['frontendHost']);
            }
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'change-operation'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $calculatorDataProvider = new ActiveDataProvider([
            'query' => (new ArithmeticOperations())->find()
        ]);

        return $this->render('index' , [
            'calculatorDataProvider' => $calculatorDataProvider,
        ]);
    }

    /**
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionChangeOperation() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $form = new OperationAvailabilityForm();

        if($form->load(Yii::$app->request->post(), '') && $form->validate() && $form->save()) {
            return [
                'success' => true,
            ];
        }
        else{
            return [
                'success' => false,
                'errors' => $form->errors
            ];
        }
    }
}
