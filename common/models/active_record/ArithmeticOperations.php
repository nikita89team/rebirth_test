<?php

namespace common\models\active_record;

use yii\db\ActiveRecord;

/**
 * Пункты навигации
 * Class ArithmeticOperations
 * @package common\models\active_record
 */
class ArithmeticOperations extends ActiveRecord
{

    public static function primaryKey()
    {
        return ['id'];
    }

    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return 'calculator_operations';
    }

}